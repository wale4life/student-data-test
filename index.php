<!doctype html >
<html>
    <head>
        <meta charset="utf-8" />
        <title>Welcome to Student Info</title>
        <link type="text/css" rel="stylesheet" href="/assets/css/bootstrap.css" />
        <!-- <link type="text/css" rel="stylesheet" href="/assets/css/main.css" /> -->
        <style>
            #app {
                display: flex;
                min-width: 100vw;
                min-height: 100vh;
                place-content: center;
                flex-direction: column;
            }
            header {
                text-align: center;
                width: 100%;
                margin-bottom: 30px;
            }
            section {
                display: block;
                width: 60%;
                margin: 10px auto;
            }
            .save { width: 100%; }
        </style>
    </head>
    <body>
        <div id="app">
            <header>
                <h1 class='title'>Student Info</h1>
                <p>
                    Kindly complete the student info form below to safeguard your info.
                </p>
            </header>
            <section id='form data'>
                <form action="#">
                    <div class="form-group">
                        <input type="text" name='name' class="form-control" placeholder='Full Name' required />
                    </div>
                    <div class="form-group">
                        <input type="text" name='matric' class="form-control" placeholder='Matriculation Number' required />
                    </div>
                    <div class="form-group">
                        <input type="text" name='department' class="form-control" placeholder='Department' required />
                    </div>
                    <div class="form-group">
                        <input type="text" name='year' class="form-control" placeholder='Year of Study' required />
                    </div>
                    <button type='submit' class='btn btn-primary save'>Save Info</button>
                </form>
            </section>
        </div>
    </body>
    <script src='/assets/js/jquery.js'></script>
    <script src='/assets/js/bootstrap.js'></script>
    <script>

        window.onload = function() {

            var form = document.querySelector('form');
            form.onsubmit  = function(e) {
                e.preventDefault();

                var btn = document.querySelector('button[type=submit]')
                btn.textContent = 'Saving please wait...'

                fetch('save.php', {
                    method: 'POST',
                    headers: {
                    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                    },
                    body: $(form).serialize(),
                }).then(res => res.json())
                .then(res => {
                    console.log(res)
                    if(res.status == 200) {
                        btn.textContent = 'Saved successfully!'
                    }
                    setTimeout(() => {
                        btn.textContent = 'Save Info';
                        form.reset();
                    }, 5000);
                }).catch(e => {
                    alert('Failure why trying to save, try again')
                    console.log(e);
                    btn.textContent = 'Save Info'
                });
            }
        }    
    </script>
</html>
