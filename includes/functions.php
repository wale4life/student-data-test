<?php
  require_once "includes/config.php";

  /** Connects to mysql and return the connection resource */
  function openConnection() {
       $con = mysqli_connect("localhost", DB_USER, DB_PASS, DB_NAME);
       if(!$con)
          throw new Exception("Unable to connect to database " . mysqli_error($con) );
	   return $con;
  }

  /**  Close mysqli connection */
  function closeConnection($resource) {
        mysqli_close($resource);
  }

  /** Perform a mysql query on the database with the provided $sql
    * @sql sql query to be performed
	* @return The mysql result resource
  */
  function sql($sql, $con) {
      $result = mysqli_query($con, $sql); 
      if(!$result) 
        throw new Exception("Query Failed: " . mysqli_error($con) );
      return $result;
  }
 
  /** Performs a query on the database
  * @sql sql query to be performed
  * @return an array of the result retrieved from database
  */
  function query($sql="", $con) {
      $result = sql($sql, $con);
	  /** Creating $data variable as an empty array to retain mysql data */
      $data = array();
        while($fetch = mysqli_fetch_array($result, MYSQLI_ASSOC) ) {
              $data[] = $fetch;
        }
      return $data;
  }
  /** Inserts into the database, returns true if sucessful
  * @sql sql query to be performed
  * @return true if insert is successful
  */
  function insert($sql="", $con) {
      try {
      $result = sql($sql, $con);
      } catch(Exception $e) {
          throw new Exception('Insertion failed: ' . $e);
      }
  }



/**
 * end of file
 *
 */