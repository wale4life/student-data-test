
$(document).ready(function (e) {
    var menu_icon = $(".icon span");
    menu_icon.toggle(function (e) {
        $(".drop_down").css({right: 0});
        $(this).css({'right': '10px', "background-position": "100% 0"});
    }, function () {
        $(".drop_down").css({right: "-500px"});
        $(this).css({'right': '40px', "background-position": "100% -54px"});
    });
    $(".drop_down").mouseenter(function () {
        menu_icon.trigger("hover");
    });
    $(".drop_down").on("mouseover", function (e) {
        menu_icon.css({"background-position": "100% 0"});
    });

    text_slide();

});

function text_slide() {
    var text = $("div.text p");
    var span = document.querySelectorAll("span.next");
    var textArray = ["Health is Wealth", "Stay Healthy", "stay happy", "get diagnozied"];
    var index = 0;

    text.css("margin-top", "0px");
    setInterval(function () {
        text.animate({"margin-top": "-150"}, 0);


        if (index >= textArray.length)
            index = 0;
        var current = span.item(index);
        $(span).each(function () {
            $(this).removeAttr("id");
            $(this).on("click", function (e) {
                console.log($(this));
                
            });
        });
        $(current).attr("id", "current_slide");


        text.text(textArray[index]);
        text.animate({"margin-top": "0"}, 500).delay(3000);

        text.animate({"margin-top": "150px"}, 500);
        index++;

        //Binding Click Event

    }, 4000);
}

    function diagonize(condition) {
      var str = "";
      var cond = condition;
      switch(cond) {
          case "malaria": 
                str += '<p><input type="checkbox" name="is_nursing" value="yes" />I\'m pregnant/nursing </p>';
                str += '<p><input type="checkbox" name="feel_pain" value="yes" />I feel pains around my muscles </p>';
                str += '<p><input type="checkbox" name="is_vomiting" value="yes" />I\'ve been vomiting</p>';
                str += '<p><input type="checkbox" name="is_high" value="yes" />I\'m running a temperature </p>';
                str += '<p><input type="checkbox" name="have_headache" value="yes" />I\'ve been feeling headache </p>';
              break;
          case "cholera":
                str += '<p><input type="checkbox" name="have_headache" value="yes" />I\'ve been feeling headache </p>';
                str += '<p><input type="checkbox" name="is_vomiting" value="yes" />I\'ve been vomiting</p>';
                str += '<p><input type="checkbox" name="is_stooling" value="yes" />I\'ve been stooling</p>';
              break;
          case "asthma":
                str += '<p><input type="checkbox" name="is_chest_pain" value="yes" />I\'ve been having chest pain</p>';
                str += '<p><input type="checkbox" name="is_coughing" value="yes" />I\'ve been experiencing chronic cough</p>';
                str += '<p><input type="checkbox" name="is_wheezing" value="yes" />I\'ve been having trouble sleeping due to coughing or wheezing</p>';
              
              break;
          case "cancer":
                str += '<p><input type="checkbox" name="is_bowel" value="yes" />I\'m experiencing a change in bowel or bladder habbits</p>';   
                str += '<p><input type="checkbox" name="cant_swallow" value="yes" />I\'m having difficulty swallowing</p>';      
                str += '<p><input type="checkbox" name="headache" value="yes" />I\'m having persistent headache</p>';   
                str += '<p><input type="checkbox" name="pain" value="yes" />I feel chronic pain in bones/any other area of the body</p>';    
              break;
          case "chicken pox":
                str += '<p><input type="checkbox" name="is_fever" value="yes" />I\'m having fever</p>';
                str += '<p><input type="checkbox" name="headache" value="yes" />Headache</p>';
                str += '<p><input type="checkbox" name="stomachache" value="yes" />Stomachache</p>';
                
              break;
      }
        str += '<p><input type="checkbox" name="med_history" value="yes" />My family have a medical history with this medical condition </p>';
        str += '<p><input type="checkbox" name="previously_treated" value="yes" />I have previously received treatment </p>';
        str += '<p><input type="checkbox" name="on_medication" value="yes" />I am currently on medication </p>';
        str += '<p><input type="checkbox" name="other_cond" value="yes" />I have other medical condition </p>';
                              
      return str;

}