<?php 
    require_once "includes/functions.php";

    $res = openConnection();
    // print_r($_POST);
    
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        
        $name = filter_var(trim($_POST["name"]), FILTER_SANITIZE_STRING);
        $matric = filter_var(trim($_POST["matric"]), FILTER_SANITIZE_STRING);
        $department = filter_var(trim($_POST["department"]), FILTER_SANITIZE_STRING);
        $year = filter_var(trim($_POST["year"]), FILTER_SANITIZE_STRING);

        $sql = "INSERT INTO students (`name`, matric_no, department, study_year)
        VALUES( '$name', '$matric', '$department', '$year' );";

        try {
            $result = insert($sql, $res);
            http_response_code(200);
            header('Content-Type: application/json');
            echo json_encode([ 'msg' => 'Successfully saved!', 'status' => 200 ]);
        } catch( Exception $e) {
            http_response_code(500);
            header('Content-Type: application/json');
            echo json_encode([ 'msg' => 'Submission failed!']);
        }
    } else {
        http_response_code(403);
        header('Content-Type: application/json');
        echo json_encode([ 'msg' => 'There was a problem with your submission, please try again.']);
    }

    closeConnection($res);



/**
 * End of File
 */