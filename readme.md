# A Demo Student Data App

## *How to Setup*
- create database with the sql script from `/setup/app.sql` 
- Copy Project to a php server.
- Setup the database info at `/includes/config.php` file
- Run the project.