CREATE DATABASE student_d_db;

use student_d_db;

CREATE table students (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `name` VARCHAR(50) NOT NULL,
    matric_no CHAR(20) NOT NULL,
    department VARCHAR(45) NOT NULL,
    study_year YEAR NOT NULL,
    date_added TIMESTAMP default CURRENT_TIMESTAMP
);